<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use App\Entity\ActiveIngridient;

class ActiveIngridientFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 4; $i++) {
            $activeIngridient = new ActiveIngridient();
            $activeIngridient->setName("active ingridient $i");
            $manager->persist($activeIngridient);

            $this->addReference("active-ingridient$i", $activeIngridient);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
