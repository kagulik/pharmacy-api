<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use App\Entity\Medication;

class MedicationFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        for ($i = 1; $i < 4; $i++) {
            $activeIngridient = $this->getReference("active-ingridient$i");
            $manufacturer = $this->getReference("manufacturer$i");
            $price = $i * 10;

            $medication = new Medication();
            $medication->setName("medication $i");
            $medication->setActiveIngridient($activeIngridient);
            $medication->setManufacturer($manufacturer);
            $medication->setPrice((float) $price);

            $manager->persist($medication);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
