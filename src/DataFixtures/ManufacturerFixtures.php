<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use App\Entity\Manufacturer;

class ManufacturerFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 4; $i++) {
            $manufacturer = new Manufacturer();
            $manufacturer->setName("manufacture $i");
            $manufacturer->setWebsite("https://manufacturer$i.com/");
            $manager->persist($manufacturer);

            $this->addReference("manufacturer$i", $manufacturer);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
