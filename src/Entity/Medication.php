<?php

namespace App\Entity;

use App\Repository\MedicationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=MedicationRepository::class)
 * @UniqueEntity("name")
 */
class Medication
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Length(
     *      min = 1,
     *      max = 100,
     *      minMessage = "Name is too short ({{ limit }} characters minimum)",
     *      maxMessage = "Name is too long ({{ limit }} characters maximum)",
     * )   
     * @Assert\Regex(
     *      pattern="/^[a-zA-Z0-9 .-]*$/",
     *      message="Name contains forbidden symbols.",
     * )
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=ActiveIngridient::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $active_ingridient;

    /**
     * @ORM\ManyToOne(targetEntity=Manufacturer::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $manufacturer;

    /**
     * @ORM\Column(type="float")
     * @Assert\Positive(
     *      message="Price must be positive number.",
     * )
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getActiveIngridient(): ?ActiveIngridient
    {
        return $this->active_ingridient;
    }

    public function setActiveIngridient(?ActiveIngridient $active_ingridient): self
    {
        $this->active_ingridient = $active_ingridient;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
