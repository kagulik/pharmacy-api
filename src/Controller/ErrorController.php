<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class ErrorController
{
    public static function show($exception, $logger)
    {
        return new JsonResponse([
            'code' => 404,
            'error' => $exception->getMessage(),
        ]);
    }
}