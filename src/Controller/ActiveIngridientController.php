<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ActiveIngridientRepository;
use OpenApi\Annotations as OA;

/**
 * @Route("/api")
 */
class ActiveIngridientController extends AbstractController
{
    /**
     * @Route("/activeIngridients", name="get_active_ingridients", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Return list of active ingridients.",
     * )
     * @OA\Get(
     *     operationId="getActiveIngridients",
     * )     
     */
    public function indexAction(ActiveIngridientRepository $activeIngridientRepository)
    {
        return $this->json($activeIngridientRepository->findAll());
    }
}