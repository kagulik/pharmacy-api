<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Repository\MedicationRepository;
use App\Repository\ActiveIngridientRepository;
use App\Repository\ManufacturerRepository;
use App\Entity\Medication;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;

/**
 * @Route("/api")
 */
class MedicationController extends AbstractController
{
    private $jsonContext = [];

    public function __construct(MedicationRepository $medicationRepository)
    {
        $this->jsonContext = [
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['__isInitialized__'],
            AbstractObjectNormalizer::SKIP_NULL_VALUES => true 
        ];
    }

    /**
     * @Route("/medications", name="get_medications", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Return list of medications with parameters as resolved objects.",
     * )
     * @OA\Get(
     *     operationId="getMedications",
     * )        
     */
    public function index(MedicationRepository $medicationRepository)
    {
        return $this->json($medicationRepository->findAll(), 200, [], $this->jsonContext);
    }

    /**
     * @Route("/medications/{id}", name="get_medication", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Get medication with object's id in object fields. Use response as request body for updating operation.",
     * )     
     * @OA\Response(
     *     response=404,
     *     description="Medication was not found.",
     * )      
     * @OA\GET(
     *     operationId="getMedication",
     * ) 
     */
    public function getMedication(MedicationRepository $medicationRepository, $id) 
    {
        $medication = $medicationRepository->find($id);
        if (!$medication) {
            return $this->json([
                'error' => 'Medication was not found.',
            ], 404);
        }

        $activeIngridientCallback = function ($innerObject) {
            return $innerObject->getId();
        };
        $manufacturerCallback = function ($innerObject) {
            return $innerObject->getId();
        };

        $this->jsonContext[AbstractNormalizer::CALLBACKS] = [
            'activeIngridient' => $activeIngridientCallback,
            'manufacturer' => $manufacturerCallback,
        ];

        return $this->json($medication, 200, [], $this->jsonContext);
    }

    /**
     * @Route("/medications/{id}", name="delete_medication", methods={"DELETE"})
     * @OA\Response(
     *     response=200,
     *     description="Deletes medication.",
     * ) 
     * @OA\Response(
     *     response=404,
     *     description="Medication was not found.",
     * )             
     * @OA\Delete(
     *     operationId="deleteMedication",
     * )       
     */
    public function deleteMedication(
        EntityManagerInterface $entityManager,
        MedicationRepository $medicationRepository, 
        $id
    ) 
    {
        $medication = $medicationRepository->find($id);
        if (!$medication) {
            return $this->json([
                'error' => 'Medication was not found.',
            ], 404);
        }

        $entityManager->remove($medication);
        $entityManager->flush();

        return $this->json([
            'message' => "Medication with id($id) was deleted.",
        ]);
    }

    /**
     * @Route("/medications/new", name="create_medication", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns id of new medication.",
     * )     
     * @OA\Response(
     *     response=404,
     *     description="Manufacturer or active ingridient were not found.",
     * )        
     * @OA\Response(
     *     response=400,
     *     description="Missing some of the parameters or they are not valid.",
     * )    
     * @OA\Post(
     *     operationId="createMedication",
     * ) 
     * @OA\RequestBody(
     *     description="{'name':'name', 'activeIngridient':'id', 'manufacturer':'id', 'price':'price'}",
     * )      
     */
    public function createMedication(
        Request $request,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        MedicationRepository $medicationRepository, 
        ActiveIngridientRepository $activeIngridientRepository,
        ManufacturerRepository $manufacturerRepository
    ) 
    {
        $data = json_decode($request->getContent(), true);

        if (!$name = $data['name'] ?? false) {
            return $this->json([
                'error' => 'Name was not set.',
            ], 400);
        }

        if (!$active_ingridient = $data['activeIngridient'] ?? false) {
            return $this->json([
                'error' => 'Active ingridient was not set.',
            ], 400);
        }

        if (!$manufacturer = $data['manufacturer'] ?? false) {
            return $this->json([
                'error' => 'Manufacturer was not set.',
            ], 400);
        }

        if (!$price = $data['price'] ?? false) {
            return $this->json([
                'error' => 'Price was not set.',
            ], 400);
        }

        if (!$active_ingridient = $activeIngridientRepository->find($active_ingridient)) {
            return $this->json([
                'error' => 'Active ingridient was not found.',
            ], 404);
        }

        if (!$manufacturer = $manufacturerRepository->find($manufacturer)) {
            return $this->json([
                'error' => 'Manufacturer was not found.',
            ], 404);
        }

        $medication = new Medication();
        $medication->setName($name);
        $medication->setPrice((float) $price);
        $medication->setActiveIngridient($active_ingridient);
        $medication->setManufacturer($manufacturer);

        $errors = $validator->validate($medication);
        if (count($errors) > 0) {
            return $this->json([
                'error' => $errors[0]->getMessage(),
            ], 400);
        }

        $entityManager->persist($medication);
        $entityManager->flush();

        $medication_id = $medication->getId();

        return $this->json([
            'id' => $medication_id,
        ]);
    }

    /**
     * @Route("/medications/{id}", name="update_medication", methods={"PUT"})
     * @OA\Response(
     *     response=200,
     *     description="Updating medication.",
     * )     
     * @OA\Response(
     *     response=404,
     *     description="Manufacturer or active ingridient were not found.",
     * )        
     * @OA\Response(
     *     response=400,
     *     description="Missing some of the parameters or they are not valid.",
     * )         
     * @OA\Put(
     *     operationId="updateMedication",
     * ) 
     * @OA\RequestBody(
     *     description="{'name':'name', 'activeIngridient':'id', 'manufacturer':'id', 'price':'price'}",
     * )      
     */
    public function updateMedication(
        Request $request,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        MedicationRepository $medicationRepository, 
        ActiveIngridientRepository $activeIngridientRepository,
        ManufacturerRepository $manufacturerRepository,
        $id
    ) 
    {
        $medication = $medicationRepository->find($id);
        if (!$medication) {
            return $this->json([
                'error' => 'Medication was not found.',
            ]);
        }

        $data = json_decode($request->getContent(), true);

        if (!$name = $data['name'] ?? false) {
            return $this->json([
                'error' => 'Name was not set.',
            ], 400);
        }

        if (!$active_ingridient = $data['activeIngridient'] ?? false) {
            return $this->json([
                'error' => 'Active ingridient was not set.',
            ], 400);
        }

        if (!$manufacturer = $data['manufacturer'] ?? false) {
            return $this->json([
                'error' => 'Manufacturer was not set.',
            ], 400);
        }

        if (!$price = $data['price'] ?? false) {
            return $this->json([
                'error' => 'Price was not set.',
            ], 400);
        }

        if (!$active_ingridient = $activeIngridientRepository->find($active_ingridient)) {
            return $this->json([
                'error' => 'Active ingridient was not found.',
            ], 404);
        }

        if (!$manufacturer = $manufacturerRepository->find($manufacturer)) {
            return $this->json([
                'error' => 'Manufacturer was not found.',
            ], 404);
        }

        $medication->setName($name);
        $medication->setPrice((float) $price);
        $medication->setActiveIngridient($active_ingridient);
        $medication->setManufacturer($manufacturer);

        $errors = $validator->validate($medication);
        if (count($errors) > 0) {
            return $this->json([
                'error' => $errors[0]->getMessage(),
            ], 400);
        }

        $entityManager->flush();
        $medication_id = $medication->getId();

        return $this->json([
            'message' => "Medication with id($medication_id) was updated.",
        ]);
    }
}
