<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ManufacturerRepository;
use OpenApi\Annotations as OA;

/**
 * @Route("/api")
 */
class ManufacturerController extends AbstractController
{
    /**
     * @Route("/manufacturers", name="get_manufacturers", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Return list of manufacturers.",
     * )
     * @OA\Get(
     *     operationId="getManufacturers",
     * )   
     */
    public function index(ManufacturerRepository $manufacturerRepository)
    {
        return $this->json($manufacturerRepository->findAll());
    }
}