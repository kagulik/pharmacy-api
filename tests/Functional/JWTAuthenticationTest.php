<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class JWTAuthenticationTest extends WebTestCase
{
    /**
     * Create a client with a default Authorization header.
     *
     * @param string $username
     * @param string $password
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function createAuthenticatedClient($username = 'username', $password = 'password')
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/login_check',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'username' => $username,
                'password' => $password,
                )
            )
        );

        $data = json_decode($client->getResponse()->getContent(), true);
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }

    public function testAuthenticatedRequest()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/doc.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testUnAuthenticatedRequest()
    {
        $client = static::createClient();
        $client->request('GET', '/api/doc.json');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }
}