<?php

 // medications/new
 //   name,price,manufacturer,ingridient are set
 //   name,price,manufacturer,ingridient are valid

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MedicationTest extends WebTestCase
{
    protected $client = null;

    /**
     * Create a client with a default Authorization header.
     *
     * @param string $username
     * @param string $password
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function getAuthenticatedClient($username = 'username', $password = 'password')
    {
        if ($this->client) {
            return $this->client;
        }

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/login_check',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'username' => $username,
                'password' => $password,
                )
            )
        );

        $data = json_decode($client->getResponse()->getContent(), true);
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));
        $this->client = $client;

        return $client;
    }

    /**
     * @group get
     */
    public function testGetMedications()
    {
        $client = $this->getAuthenticatedClient();
        $client->request('GET', '/api/medications');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
    }

    /**
     * @group get
     */
    public function testGetMedication()
    {
        $client = $this->getAuthenticatedClient();
        $client->request('GET', '/api/medications');

        $medications = json_decode($client->getResponse()->getContent(), true);
        $medication_id = $medications[0]['id'];

        $client->request('GET', "/api/medications/$medication_id");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
    }

    /**
     * @group get
     */
    public function testGetInvalidMedication()
    {
        $client = $this->getAuthenticatedClient();
        $medication_id = 'invalid_id';

        $client->request('GET', "/api/medications/$medication_id");
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * @group delete
     */
    public function testDeleteMedication()
    {
        $client = $this->getAuthenticatedClient();
        $client->request('GET', '/api/medications');

        $medications = json_decode($client->getResponse()->getContent(), true);
        $medication_id = $medications[0]['id'];

        $client->request('DELETE', "/api/medications/$medication_id");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());

        // medication does not exist
        $client->request('GET', "/api/medications/$medication_id");
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * @group delete
     */
    public function testDeleteInvalidMedication()
    {
        $client = $this->getAuthenticatedClient();
        $medication_id = 'invalid_id';

        $client->request('DELETE', "/api/medications/$medication_id");
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * @group update
     */
    public function testUpdateMedication()
    {
        $client = $this->getAuthenticatedClient();
        $client->request('GET', '/api/medications');

        $medications = json_decode($client->getResponse()->getContent(), true);
        $medication_id = $medications[0]['id'];

        $client->request('GET', "/api/medications/$medication_id");
        $medication = json_decode($client->getResponse()->getContent(), true);

        $new_name = $medication['name'] . ' 1';
        $medication['name'] = $new_name;

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());

        $client->request('GET', "/api/medications/$medication_id");
        $medication = json_decode($client->getResponse()->getContent(), true);
        $this->assertSame($new_name, $medication['name']);
    }

    /**
     * @group update
     */
    public function testUpdateMedicationWithInvalidParameters()
    {
        $client = $this->getAuthenticatedClient();
        $client->request('GET', '/api/medications');

        $medications = json_decode($client->getResponse()->getContent(), true);
        $medication_id = $medications[0]['id'];

        $client->request('GET', "/api/medications/$medication_id");
        $unmodified_medication = json_decode($client->getResponse()->getContent(), true);

        // no manufacturer
        $medication = $unmodified_medication;
        unset($medication['manufacturer']);

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // no active ingridient
        $medication = $unmodified_medication;
        unset($medication['activeIngridient']);

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // no manufacturer
        $medication = $unmodified_medication;
        unset($medication['manufacturer']);

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // no name
        $medication = $unmodified_medication;
        $new_name = 'invalid_name!?$';
        unset($medication['name']);

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // no price 
        $medication = $unmodified_medication;
        $new_price = 0;
        unset($medication['price']);

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // active ingridient
        $medication = $unmodified_medication;
        $new_active_ingridient = 'invalid_active_ingridient';
        $medication['activeIngridient'] = $new_active_ingridient;

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // manufacturer
        $medication = $unmodified_medication;
        $new_manufacturer = 'invalid_manufacturer';
        $medication['manufacturer'] = $new_manufacturer;

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // name
        $medication = $unmodified_medication;
        $new_name = 'invalid_name!?$';
        $medication['name'] = $new_name;

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // price 
        $medication = $unmodified_medication;
        $new_price = 0;
        $medication['price'] = $new_price;

        $client->request(
            'PUT',
            "/api/medications/$medication_id",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        $client->request('GET', "/api/medications/$medication_id");
        $medication = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotSame($medication['activeIngridient'], $new_active_ingridient);
        $this->assertNotSame($medication['manufacturer'], $new_manufacturer);
        $this->assertNotSame($medication['name'], $new_name);
        $this->assertNotSame($medication['price'], $new_price);
   }

    /**
     * @group create
     */
    public function testCreateMedication()
    {
        $client = $this->getAuthenticatedClient();
        $client->request('GET', '/api/medications');

        $medications = json_decode($client->getResponse()->getContent(), true);
        $medication_id = $medications[0]['id'];

        $client->request('GET', "/api/medications/$medication_id");
        $medication = json_decode($client->getResponse()->getContent(), true);
        $medication['name'] = $medication['name'] . ' 1';

        $client->request(
            'POST',
            "/api/medications/new",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        

        $this->assertJson($client->getResponse()->getContent());
        $new_medication = json_decode($client->getResponse()->getContent(), true);

        $client->request('GET', "/api/medications/{$new_medication['id']}");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
   }

    /**
     * @group create
     */
    public function testCreateMedicationWithInvalidParameters()
    {
        $client = $this->getAuthenticatedClient();
        $client->request('GET', '/api/medications');

        $medications = json_decode($client->getResponse()->getContent(), true);
        $medications_amount_before = count($medications);
        $medication_id = $medications[0]['id'];

        $client->request('GET', "/api/medications/$medication_id");
        $unmodified_medication = json_decode($client->getResponse()->getContent(), true);

        // no active ingridient
        $medication = $unmodified_medication;
        unset($medication['activeIngridient']);

        $client->request(
            'POST',
            "/api/medications/new",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // no manufacturer
        $medication = $unmodified_medication;
        unset($medication['manufacturer']);

        $client->request(
            'POST',
            "/api/medications/new",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // no name
        $medication = $unmodified_medication;
        $new_name = 'invalid_name!?$';
        unset($medication['name']);

        $client->request( 
            'POST',
            "/api/medications/new",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // no price 
        $medication = $unmodified_medication;
        $new_price = 0;
        unset($medication['price']);

        $client->request(
            'POST',
            "/api/medications/new",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // active ingridient
        $medication = $unmodified_medication;
        $new_active_ingridient = 'invalid_active_ingridient';
        $medication['activeIngridient'] = $new_active_ingridient;

        $client->request(
            'POST',
            "/api/medications/new",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(404, $client->getResponse()->getStatusCode());



        // manufacturer
        $medication = $unmodified_medication;
        $new_manufacturer = 'invalid_manufacturer';
        $medication['manufacturer'] = $new_manufacturer;

        $client->request(
            'POST',
            "/api/medications/new",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // name
        $medication = $unmodified_medication;
        $new_name = 'invalid_name!?$';
        $medication['name'] = $new_name;

        $client->request(
            'POST',
            "/api/medications/new",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // price 
        $medication = $unmodified_medication;
        $new_price = 0;
        $medication['price'] = $new_price;

        $client->request(
            'POST',
            "/api/medications/new",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($medication, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)
        );        
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        $client->request('GET', '/api/medications');
        $medications = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals($medications_amount_before, count($medications));
   }
}