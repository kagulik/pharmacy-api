# Installation

1. Download docker repo

```bash
$ git clone https://github.com/eko/docker-symfony [installation_folder]
```

2. Add host as described [there](https://github.com/eko/docker-symfony#installation)

```bash
$ echo '127.0.0.1 symfony.localhost' | sudo tee -a /etc/hosts
```

3. Download app repo
```bash
$ cd [installation_folder] && git clone https://bitbucket.org/kagulik/toplyvoua-task symfony
```

4. Start docker
```bash
$ docker-compose up -d
```

5. Download dependencies for appropriate php version in docker repo
```bash
$ docker-compose exec php composer install
```


# Using

1. Make migration and migrate
```bash
$ cd [installation_folder] && docker-compose exec php php bin/console make:migration
$ docker-compose exec php php bin/console doctrine:migrations:migrate
```

2. Run fixtures dataset
```bash
$ docker-compose exec php bin/console doctrine:fixtures:load
```

3. Generate ssh keys as written [here](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md#getting-started)
Take passphrase from `JWT_PASSPHRASE` env variable or set yours.

```bash
$ cd symfony
$ mkdir -p config/jwt
$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

4. Change private key permnissions (**THAT IS BAD**)
```bash
$ chmod go+r config/jwt/private.pem
```

5. Obtain jwt token
```bash
$ curl -X POST -H "Content-Type: application/json" http://symfony.localhost/api/login_check -d '{"username":"username","password":"password"}'
```

6.Have a look at the docs
```bash
$ curl -H "Authorization: Bearer {token}" -H "Content-Type: application/json" -X GET http://symfony.localhost/api/doc.json
```

7. Use app
```bash
$ curl -H "Authorization: Bearer {token}" -H "Content-Type: application/json" -X GET http://symfony.localhost/api/medications
```


# Testing

1. Download app
```bash
$ git clone https://bitbucket.org/kagulik/toplyvoua-task [app_folder]
```

2. Install dependencies
```bash
$ cd [app_folder] && composer install
```

3. Generate ssh keys for testing as said [here](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/3-functional-testing.md)
```bash
$ mkdir -p config/jwt
$ openssl genrsa -out config/jwt/private-test.pem -aes256 4096
$ openssl rsa -pubout -in config/jwt/private-test.pem -out config/jwt/public-test.pem
```

4. Create `.env.local` file and put inside
`DATABASE_URL="sqlite:///%kernel.project_dir%/var/app.db.sqlite"`

5. Create and run migration
```bash
$ php bin/console make:migration
$ php bin/console doctrine:migrations:migrate
```

6. Run fixtures dataset
```bash
$ php bin/console doctrine:fixtures:load
```

7. Switch to test environment

8. Run tests
```bash
$ php bin/phpunit
```


# Troubleshooting

* Unable to write in the logs directory (/var/www/symfony/var/log) (or in /var/www/symfony/var/cache)
```bash
$ docker-compose exec php php bin/console cache:clear
```

If it didn't work out for you then try this, as described [here](https://github.com/eko/docker-symfony/issues/104#issuecomment-451649872)
```bash
$ sudo chmod 0777 var/log -R
```